<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'dungle';
        $user->email = 'letdung86@gmail.com';
        $user->username= 'dungle';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach(Role::where('name', 'user')->first());
 
        $admin = new User;
        $admin->name = 'anhle';
        $admin->email = 'ltheanh2@gmail.com';
        $user->username= 'anhlt';
        $admin->password = bcrypt('12345');
        $admin->save();
        $admin->roles()->attach(Role::where('name', 'admin')->first());
    }
}
